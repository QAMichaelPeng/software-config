SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
CONFIG_DIR=$HOME/.config/yazi/
[[ -d "$CONFIG_DIR" ]] || mkdir -p "$CONFIG_DIR"
cp "$SCRIPT_DIR"/*.toml $CONFIG_DIR

