import json
import os
import argparse
from git import Repo

def exec_command(cmd):
    res = os.system(cmd)
    if res:
        eprint("exec \"%s\" error", cmd)
        sys.exit(-1)

def exec_commands(cmds):
    exec_command(" && ".join(cmds))

def get_branch():
    with open(".branch") as branch_file:
        branch_config = json.load(branch_file)
        master, working = branch_config["master"], branch_config["working"]
        return master, working

def submit_func():
    master, working = get_branch()
    cmds= [ "git checkout %s" % master,
            "git pull --rebase",
            "git checkout %s" % working,
            "git rebase %s" % master,
            "git checkout %s" % master,
            "git merge --no-ff  %s" % working,
            "git push",
            "git checkout %s" % working,
            "git rebase %s" % master
            ]
    exec_commands(cmds)

def pull_func():
    master, working = get_branch()
    repo = Repo(".")
    is_dirty = repo.is_dirty()
    cmds = [ ]
    if is_dirty:
        cmds.append("git stash")
    cmds.extend([
        "git checkout %s" % master,
        "git pull --rebase",
        "git checkout %s" % working,
        "git rebase %s" % master,
        ])
    if is_dirty:
        cmds.append("git stash pop")
    exec_commands(cmds)


parser = argparse.ArgumentParser(description="git helper")
subparsers = parser.add_subparsers()
submit = subparsers.add_parser("submit", help="submit")
submit.set_defaults(func=submit_func)
pull = subparsers.add_parser("pull", help="pull")
pull.set_defaults(func=pull_func)
args = parser.parse_args()
args.func()
