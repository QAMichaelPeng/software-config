if empty(glob('~/.vim/autoload/plug.vim'))
    " -f: fail silently
    " -L: while get 3XX, resent to new location
    " -o: output file
    " --create-dirs: create necessary dirs while used in conjuction with -o 
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source ~/.vimrc
endif

" Leader character and map
let mapleader = ","
" predefined shortcut for leaderF
let g:Lf_ShortcutF="<leader>lf"
let g:Lf_ShortcutB="<leader>lb"

call plug#begin('~/.vim/bundle')
    " NERDTree, file expolore for vim
    Plug 'scrooloose/nerdtree'
    " Buf explore easily
    Plug 'jlanzarotta/bufexplorer'
    " Copy path and file name to clipboard
    Plug 'vim-scripts/copypath.vim'
    " CJK encoding
    Plug 'QAMichaelPeng/FencView.vim'
    " Syntax highlight debug tool
    Plug 'QAMichaelPeng/hi-link-trace'
    " close tag automatically
    Plug 'alvan/closetag.vim'
    Plug 'altercation/vim-colors-solarized'
    Plug 'gerw/vim-HiLinkTrace'
    Plug 'skywind3000/asyncrun.vim'
    " Track the snippets engine.
    Plug 'SirVer/ultisnips'
    " Snippets are separated from the engine. Add this if you want them:
    Plug 'honza/vim-snippets'
    " Denite plugin of plugin
    Plug 'Shougo/denite.nvim'
    Plug 'vim-airline/vim-airline'
    Plug 'airblade/vim-gitgutter'
    Plug 'tpope/vim-fugitive'
    Plug 'Yggdroot/LeaderF'
    " vim text objects
    Plug 'kana/vim-textobj-entire'
    Plug 'kana/vim-textobj-user'
    " commentary
    Plug 'tpope/vim-commentary'
call plug#end()

set tabstop=4 " 4 spaces for tab
set shiftwidth=4 " indent 4
set expandtab " expand tab to space
" incremental search, highlight search, case smart
set hlsearch " highlight search result
set incsearch " incemental search
set ignorecase " ignore case
set smartcase " lower case for ignore case, upper case for case sensitive matching
set cursorline " highlight current line
set number " line number
set relativenumber " relative line number
set paste " turn on paste mode by default, I always forget to set paste while pasting huge amount of data
set list " whiltespace characters mapping
set listchars=tab:›\ ,trail:•,extends:#,nbsp:. " Highlight problematic whitespace
set foldenable " enable fold by default
set foldmethod=syntax " fold by syntax
set splitright " puts new vsplit windows to the right of the current
set splitbelow " puts new split windows to the bottom of the current
set wrap " enable wrap for long line
set autoindent " copy current indent for new line
set smartindent " do smart autoindenting for {} block for newline
set nojoinspaces " no extra join spaces
set hidden
set history=1000
cnoremap <C-p> <Up>
cnoremap <C-n> <Down>

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-l>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"


function! CopyAll()
    mark `
    execute 'normal! ggVG"+y'
    normal ``
endfunction

function! CopyLine()
    execute 'normal! m`V"+y``'
endfunction

function! CopyWord()
    silent execute 'normal! m`viw"+y``'
endfunction

function! CopyWORD()
    silent execute 'normal! m`viW"+y``'
endfunction

" edit vim rc
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
" soruce vimrc
nnoremap <leader>sv :source $MYVIMRC<cr>
" copy selected in visual mode
vnoremap <Leader>c "+y
nnoremap <Leader>ca :call CopyAll()<cr>
nnoremap <Leader>cl :call CopyLine()<cr>
nnoremap <Leader>cw :call CopyWord()<cr>
nnoremap <Leader>cW :call CopyWORD()<cr>
nnoremap <Leader>v "+p
noremap <leader>lga :Leaderf gtags<cr>
noremap <leader>lgc :Leaderf gtags --current-buffer<cr>
" After pressing leader the guide buffer will pop up when there are no further keystrokes within timeoutlen.
nnoremap <silent> <leader> :WhichKey '<Space>'<CR>

" customize for local
let b:localvim=expand("~/.local/etc/local.vim")
if filereadable(b:localvim)
    execute "source" b:localvim
endif

" see Oil and vinegar - split windows and the project drawer
" http://vimcasts.org/blog/2013/01/oil-and-vinegar-split-windows-and-project-drawer/
let NERDTreeHijackNetrw=1

augroup QAMichaelPeng
    autocmd!
    " Ruby is an oddball in the family, use special spacing/rules
    autocmd FileType ruby setlocal ts=2 sts=2 sw=2 norelativenumber nocursorline
augroup end

python3 << en
import vim
def MDGetMode():
    return vim.eval("mode()")

def MDGetSelectionStart():
    return vim.eval("getpos(\"'<'\")")[1:3]

def MDGetSelectionEnd():
    return vim.eval("getpos(\"'>'\")")[1:3]

def MDGetLines(start_line, end_line):
    return vim.eval("getline(%s, %s)"%(start_line, end_line))

def MDEscape(line):
    """
    The code might be buggy if the input line has backslash
    Ignore for now
    """
    return line.replace('"', '\\"')

def MDUpdateLine(line_number, line, sep):
    fields = line.split(sep)
    new_line =  "| " + " | ".join(fields) + " |"
    vim.eval("setline(%s, \"%s\")" % (line_number, MDEscape(new_line)))

def MDVisualSelection():
    if MDGetMode()=="n":
        line_start, _ = MDGetSelectionStart()
        line_end, _  = MDGetSelectionEnd()
        line_start, line_end = int(line_start), int(line_end)
        lines = MDGetLines(line_start, line_end)
        if len(lines)==0:
            return

        head = lines[0]
        sep = "," if head.find("\t")==-1 else "\t"
        head_fields = head.split(sep)
        under_header = "| " + " | ".join(["--"] * len(head_fields)) + " |"
        for i in range(line_start, line_end+1):
            MDUpdateLine(i, lines[i-line_start], sep)
        vim.eval("append(%s, \"%s\")" % (line_start, under_header))

en

vmap <silent> <leader>md :py3 MDVisualSelection()<CR>
