for f in anaconda3/bin; do
    if [[ -d "$HOME/$f" ]]; then
        export PATH="$HOME/$f":$PATH
    fi
done

for env in py36 py35;
do
    if [ -d "$HOME/anaconda3/envs/$env" ];
    then
        # see https://stackoverflow.com/questions/57698497/conda-does-not-set-paths-when-activating-environment/57707711#57707711
        # see https://unix.stackexchange.com/questions/366553/tmux-is-causing-anaconda-to-use-a-different-python-source
        source activate $env
        conda deactivate
        conda activate $env
        break
    fi
done
