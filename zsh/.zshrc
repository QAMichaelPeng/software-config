##########
# init corp zshrc
##########

LOCAL_ZSH_CONFIG_DIRECTORY="$HOME/.local/etc/zsh"
# initial at the beginning to access it later in this script
[ -f "$LOCAL_ZSH_CONFIG_DIRECTORY/corp_init.zshrc" ] && source "$LOCAL_ZSH_CONFIG_DIRECTORY/corp_init.zshrc"

##########
# Initialize antigen
##########

# Antigen: https://github.com/zsh-users/antigen
ANTIGEN="$HOME/.local/bin/antigen.zsh"

# Install antigen.zsh if not exist
if [ ! -f "$ANTIGEN" ]; then
	echo "Installing antigen ..."
	[ ! -d "$HOME/.local" ] && mkdir -p "$HOME/.local" 2> /dev/null
	[ ! -d "$HOME/.local/bin" ] && mkdir -p "$HOME/.local/bin" 2> /dev/null
	# [ ! -f "$HOME/.z" ] && touch "$HOME/.z"
	URL="http://git.io/antigen"
	TMPFILE="/tmp/antigen.zsh"
	if [ -x "$(which curl)" ]; then
		curl -L "$URL" -o "$TMPFILE"
	elif [ -x "$(which wget)" ]; then
		wget "$URL" -O "$TMPFILE"
	else
		echo "ERROR: please install curl or wget before installation !!"
		exit
	fi
	if [ ! $? -eq 0 ]; then
		echo ""
		echo "ERROR: downloading antigen.zsh ($URL) failed !!"
		exit
	fi;
	echo "move $TMPFILE to $ANTIGEN"
	mv "$TMPFILE" "$ANTIGEN"
fi

source "$ANTIGEN"

antigen use oh-my-zsh

# default bundles from (robbyrussell's oh-my-zsh)
antigen bundle git
#antigen bundle tmux
antigen bundle ag
antigen bundle z

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting
#antigen theme robbyrussell
antigen apply
# Load the theme.

PROMPT="%2c $ "


# syntax color definition
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern)

typeset -A ZSH_HIGHLIGHT_STYLES

# ZSH_HIGHLIGHT_STYLES[command]=fg=white,bold
# ZSH_HIGHLIGHT_STYLES[alias]='fg=magenta,bold'

ZSH_HIGHLIGHT_STYLES[default]=none
ZSH_HIGHLIGHT_STYLES[unknown-token]=fg=009
ZSH_HIGHLIGHT_STYLES[reserved-word]=fg=009,standout
ZSH_HIGHLIGHT_STYLES[alias]=fg=cyan,bold
ZSH_HIGHLIGHT_STYLES[builtin]=fg=cyan,bold
ZSH_HIGHLIGHT_STYLES[function]=fg=cyan,bold
ZSH_HIGHLIGHT_STYLES[command]=fg=white,bold
ZSH_HIGHLIGHT_STYLES[precommand]=fg=white,underline
ZSH_HIGHLIGHT_STYLES[commandseparator]=none
ZSH_HIGHLIGHT_STYLES[hashed-command]=fg=009
ZSH_HIGHLIGHT_STYLES[path]=fg=214,underline
ZSH_HIGHLIGHT_STYLES[globbing]=fg=063
ZSH_HIGHLIGHT_STYLES[history-expansion]=fg=white,underline
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]=none
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]=none
ZSH_HIGHLIGHT_STYLES[back-quoted-argument]=none
ZSH_HIGHLIGHT_STYLES[single-quoted-argument]=fg=063
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]=fg=063
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]=fg=009
ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]=fg=009
ZSH_HIGHLIGHT_STYLES[assign]=none
EMSDK_PATH="$HOME/Projects/3rd/emsdk"
JENV_PATH="$HOME/.jenv"

paths=(
    "/opt/homebrew/bin"
    "/opt/homebrew/opt/qt@5/bin"
    "$HOME/bin"
    "$HOME/.local/bin"
    "/home/linuxbrew/.linuxbrew/bin"
)
# can't use path as variable for below
# have to use p
:<<'COMMENT'
The ~/.zshrc file is a configuration file used by the Zsh shell. It contains settings and commands that are executed whenever a new terminal session is started.

The PATH environment variable is used by the shell to determine the directories where executable files are located. When a user enters a command in the terminal, the shell searches for the corresponding executable file in the directories listed in the PATH variable.

The line of code you provided, PATH="${PATH:+"$PATH:"}$p", appends the value of the variable $p to the PATH environment variable. Here's how it works:
 ${PATH:+...} expands to the contents of the ... string only if the PATH variable is set and not empty. If PATH is not set or is empty, this expression expands to nothing.
"${PATH:+$PATH:}" expands to the value of the PATH variable with a colon (:) appended to the end if PATH is set and not empty. If PATH is not set or is empty, this expression expands to an empty string.
 $p is the value you want to append to the PATH variable.
So the overall effect of the line of code is to append the value of $p to the PATH variable, separated by a colon (:), only if PATH is set and not empty. If PATH is not set or is empty, the value of $p is used as the new value of PATH. This ensures that the existing value of PATH is preserved while also adding the new directory specified by $p to the search path for executable files.
COMMENT

for p in ${paths[@]}; do
    if [[ -d "$p" ]] && [[ ":$PATH:" != *":$p:"* ]]; then
        PATH="$p${PATH:+":$PATH:"}"
    fi
done

if [[ -d "${JENV_PATH}" ]]; then
    PATH="${JENV_PATH}/bin${PATH:+":$PATH:"}"
    eval "$(jenv init -)"
fi

if [[ -d "${EMSDK_PATH}" ]]; then
	export EMSDK_QUIET=1
	source ${EMSDK_PATH}/emsdk_env.sh
	unset EMSDK_QUIET
fi


cmds=(
"vim"
"java"
)

:<<'COMMENT'
Some command might appear under different paths of $PATH components
Choose the prefered one with alias
COMMENT

for cmd in ${cmds[@]}; do
    if [[ -f "/opt/homebrew/bin/${cmd}" ]]; then
        alias ${cmd}=/opt/homebrew/bin/${cmd}
    fi
done

# Node version manager 
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"

[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

if [[ -d "$HOME/.pyenv" ]]; then
    export PYENV_ROOT="$HOME/.pyenv"
    command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
fi

if [[ -f "$HOME/.cargo/env" ]]; then
    source $HOME/.cargo/env
else 
    # https://rustup.rs/
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
fi


which fzf > /dev/null
if [[ $?  -eq 0 ]] then
    source <(fzf --zsh)
    which ag > /dev/null
    if [ $? -eq 0 ]; then
        export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -l -g ""'
    fi
    # https://github.com/junegunn/fzf/wiki/examples
    # fdp - cd to selected parent directory, 'fzf directory parent'
    fdp() {
      local declare dirs=()
      get_parent_dirs() {
        if [[ -d "${1}" ]]; then dirs+=("$1"); else return; fi
        if [[ "${1}" == '/' ]]; then
          for _dir in "${dirs[@]}"; do echo $_dir; done
        else
          get_parent_dirs $(dirname "$1")
        fi
      }
      local DIR=$(get_parent_dirs $(realpath "${1:-$PWD}") | fzf-tmux --tac)
      cd "$DIR"
    }
    
    # tm - create new tmux session, or switch to existing one. Works from within tmux too. (@bag-man)
    # `tm` will allow you to select your tmux session via fzf.
    # `tm irc` will attach to the irc session (if it exists), else it will create it.
    tm() {
      [[ -n "$TMUX" ]] && change="switch-client" || change="attach-session"
      if [ $1 ]; then
        tmux $change -t "$1" 2>/dev/null || (tmux new-session -d -s $1 && tmux $change -t "$1"); return
      fi
      session=$(tmux list-sessions -F "#{session_name}" 2>/dev/null | fzf --exit-0) &&  tmux $change -t "$session" || echo "No sessions found."
    }
fi

