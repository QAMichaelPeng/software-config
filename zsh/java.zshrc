declare -a jdk_versions=("jdk1.8.0_45.jdk")
for jdk_version in "${jdk_versions[@]}"
do
    jdk_home="/Library/Java/JavaVirtualMachines/$jdk_version/Contents/Home/"
    if [[ -d "$jdk_home" ]]; then
        export JAVA_HOME="$jdk_home"
        break
    fi
done

